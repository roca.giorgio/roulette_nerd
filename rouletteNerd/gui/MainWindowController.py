#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import sys
from pathlib import Path

from gui.PieItem import PieItem
from gui.ui.mainGUi import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QBrush, QColor, QPen
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from utils import Utils


class MainWindowController(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindowController, self).__init__()
        print("init MyWindow")
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.label.setText("Roulette Nerd")
        self.ui.label.setFont(QtGui.QFont("Helvetica [Cronyx]", 30))
        self.ui.pushButton.setFont(QtGui.QFont("Helvetica [Cronyx]", 30))
        self.ui.pushButton.setText("Start!!!")
        self.ray = min(self.ui.graphicsView.frameGeometry().height(),
                       self.ui.graphicsView.frameGeometry().width())
        print("size: " + str(self.ui.graphicsView.height()) + "x" +
              str(self.ray))
        self.ui.pushButton.clicked.connect(self.defaultAction)
        self.ui.action_Scan.triggered.connect(
            lambda state: print("Scan CLICK"))
        self.ui.action_Exit.triggered.connect(lambda state: sys.exit())
        self.ui.actionabout.triggered.connect(
            lambda state: print("About CLICK"))

    def createCircle(self, items):
        num = len(items)
        color = self.getColors(num)
        print("CreateCirlce ray={}, num={}".format(self.ray, num))
        scene = QtWidgets.QGraphicsScene()
        coord = QtCore.QRectF(-0, -0, self.ray, self.ray)
        pen = QPen(QColor(102, 51, 0), 5)
        brush = QBrush(QtCore.Qt.blue)
        incr = 360 / num
        start = 0
        end = 0 + incr
        print("CALCOLO RAGGI Icr::{}, start::{}, end::{}".format(
            incr, start, end))

        for i in range(num):
            brush.setColor(color[i])
            pie = QtWidgets.QGraphicsEllipseItem(coord)
            pie.setData(0, items[i])
            pie.setStartAngle(start * 16)
            pie.setSpanAngle(end * 16)
            pie.setPen(pen)
            pie.setBrush(brush)
            scene.addItem(pie)
            start += incr

        self.wheel = PieItem(self.ui.graphicsView)
        #        #triangle = createCursor(pen, brush)
        #        #scene.addPolygon(triangle, pen, brush)
        self.ui.graphicsView.setScene(scene)

    def createCursor(ray, pen, brush):
        triangle = QtGui.QPolygonF()
        pen.setColor(QtCore.Qt.white)
        pen.setWidth(2)
        brush.setColor(QtCore.Qt.black)
        late = ray / 2
        triangle.append(QtCore.QPointF(late - 20., 0.))
        triangle.append(QtCore.QPointF(late, 25.))
        triangle.append(QtCore.QPointF(late + 20., 0.))
        return triangle

    def getColors(self, lenght):
        color = []
        for i in range(lenght + 1):
            color.append(self.randomColor())
        return color

    def randomColor(self):
        number = []
        for count in range(3):
            number.append(random.randrange(0, 255))
        return QColor(number[0], number[1], number[2])

    def defaultAction(self):
        print("CLICK on START!!!!")

    def setButtonAction(self, function):
        self.ui.pushButton.clicked.connect(function)

    def setListText(self, textsModel):
        print("SetListTable: {}".format(textsModel))
        self.ui.tableView.setModel(textsModel)

    def popupMessage(self, item):
        msg = """
            Il Progetto estratto è:\n{}
            \nClicca su Open per aprire la cartella
        """
        msg = msg.format(item.name)
        box = QMessageBox()
        box.setWindowTitle("Roulette Nerd")
        box.setText(msg)
        box.setDetailedText(item.path)
        box.setStandardButtons(QMessageBox.Cancel | QMessageBox.Open
                               | QMessageBox.Retry)
        box.setDefaultButton(QMessageBox.Open)
        #        box.buttonClicked.connect(self.popup_button)
        self.itemExtract = item
        result = box.exec()
        self.popup_button(result)

    def popup_button(self, i):
        print("PopUp for item::\n{} -> {}".format(self.itemExtract,
                                                  dir(self.itemExtract)))
        if (i == QMessageBox.Open):
            print("Open Dir")
            Utils.openDir(self.itemExtract.path)
        elif (i == QMessageBox.Retry):
            print("Retry")
            self.startWheel()
        else:
            print("PopUp Button clicked :: {}".format(i))
            print("Class :: {}".format(i.__class__))

    def openFile(self):
        home = str(Path.home())
        options = QFileDialog.Options()
        options |= QFileDialog.ShowDirsOnly
        selectDir = QFileDialog.getExistingDirectory(self, "Open Directory",
                                                     home, options)
        if selectDir:
            print(selectDir)

    def endAnimationFunction(self):
        print("Animation FINISH!!!!")
        self.ui.pushButton.setDisabled(False)
        item = self.wheel.item.itemAt(self.ray, self.ray - 13)
        if item is not None:
            print("Item Select is {} \nDATA_VALUE: {}".format(
                str(item), str(item.data(0))))
            self.popupMessage(item.data(0))
        else:
            print("Item is None o null. Retry")
            self.startWheel()

    def startWheel(self):
        duration = random.randrange(5, 360) * random.randrange(1, 12)
        print("Start Roulette")
        self.ui.pushButton.setEnabled(False)
        animation = QtCore.QPropertyAnimation(self.wheel, b"angle", self)
        animation.setStartValue(0)
        animation.setEndValue(duration)
        animation.finished.connect(self.endAnimationFunction)
        animation.start()
