#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QObject, pyqtProperty
from PyQt5.QtGui import QTransform


class PieItem(QObject):
    def __init__(self, item=None):
        super(QtCore.QObject, self).__init__()
        self.item = item
        self._angle = 0

    @pyqtProperty(int)
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, value):
        self._angle = value
        #self.item.rotate(self._angle)
        print("set angle" + str(value))
        scene = self.item.scene()
        gr = scene.createItemGroup(scene.selectedItems())
        offset = gr.sceneBoundingRect().center()
        transform = QTransform()
        transform.translate(offset.x(), offset.y())
        transform.rotate(self._angle)
        transform.translate(-offset.x(), -offset.y())
        self.item.setTransform(transform)
