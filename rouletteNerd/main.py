#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys

from gui.MainWindowController import MainWindowController
from PyQt5 import QtWidgets

from utils import Utils


def main():
    app = QtWidgets.QApplication([])
    args = cliParser()
    items = Utils.getItems(args)
    gui = MainWindowController()
    gui.setButtonAction(gui.startWheel)
    gui.setListText(Utils.testTable(items))
    gui.createCircle(items)

    gui.show()
    sys.exit(app.exec())


def cliParser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s",
                        "--scan",
                        metavar="path",
                        help="scanning a directory for search the projects",
                        default=None)
    parser.add_argument("-d",
                        "--depth",
                        metavar="level",
                        type=int,
                        help="depth of a recursive scannig.(default = 2)",
                        default=2)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
