#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt5 import QtCore
from PyQt5.QtGui import QBrush


class Item():
    def __init__(self, name=None, path=None, score=None, color=None):
        self.col = 2
        self.name = name
        self.path = path
        self.score = score
        self.color = color

    def path(self):
        return self._path


class TableItemModel(QtCore.QAbstractTableModel):
    def __init__(self, parent=None, listItem=None):
        super(TableItemModel, self).__init__()
        print("INIT TABLE with {} object".format(
            len(listItem) if listItem is not None else -1))
        self.listItem = listItem

    def update(self, dataIn):
        print('Updating Model')
        self.listItem = dataIn
        print('Datatable : {0}'.format(len(self.listItem)))

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.listItem)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return int(self.listItem[0].col if len(self.listItem) > 0 else 0)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        #print 'Data Call'
        #print index.column(), index.row()
        if role == QtCore.Qt.DisplayRole:
            i = index.row()
            j = index.column()
            return '{0}'.format(self.get_value(i, j))
        elif role == QtCore.Qt.BackgroundRole and index.column() == 2:
            return QBrush(self.get_value(index.row(), index.column()))
        else:
            return QtCore.QVariant()

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled

    def get_value(self, i, j):
        item = self.listItem[i]
        if j == 0:
            return str(item.name)
        elif j == 1:
            return item.path
