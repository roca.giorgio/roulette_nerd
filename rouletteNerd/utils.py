#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pickle
import platform
import subprocess

from model.item import Item, TableItemModel


class Utils:
    def openDir(path):
        print("OPEN DIR::{}".format(path))
        filepath = os.path.realpath(path)
        if platform.system() == 'Darwin':  # macOS
            subprocess.call(('open', filepath))
        elif platform.system() == 'Windows':  # Windows
            os.startfile(filepath)
        else:  # linux variants
            subprocess.call(('xdg-open', filepath))

    def getItems(args):
        items = []
        if (args.scan is not None and 0 < args.depth):
            print("Scan Mode")
            items = Utils.scanDir(args.scan, args.depth)
            Utils.saveItems(items)
        else:
            print("Load MODE")
            items = Utils.loadItems()
        return items

    def scanDir(path, depth):
        items = []
        for entry in os.scandir(path):
            print(entry)
            if not entry.name.startswith('.') and entry.is_dir():
                items.append(Item(entry.name, entry.path, 1))
        if (0 < (depth - 1)):
            print("Scan Recorsive: depth={}".format(depth - 1))
            items_2 = []
            for item in items:
                items_2.append(Utils.scanDir(item.path, depth - 1))
            items = items_2
        print("SCAN retun a array:{}".format(len(items)))
        return items

    def saveItems(items):
        print("SAVE to File {} object".format(len(items)))
        with open("bin.dat", "wb") as f:
            pickle.dump(items, f)

    def loadItems():
        print("LOAD from file")
        try:
            with open("bin.dat", "rb") as f:
                items = pickle.load(f)
                print("LOAD {} object".format(len(items)))
        except Exception as e:
            print("EE:{}".format(e))
            items = []
        print("return {} object".format(len(items)))
        return items

    def testTable(items):
        print("table from {} object".format(len(items)))
        textListModel = TableItemModel(listItem=items)
        return textListModel
