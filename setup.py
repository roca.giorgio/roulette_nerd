#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
    name="rouletteNerd",  # Replace with your own username
    version="0.0.1",
    author="giorgio",
    author_email="",
    description="A example package for learning",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/roca.giorgio/roulette_nerd",
    packages=setuptools.find_packages(
        include=['rouletteNerd', 'rouletteNerd/*']),
    entry_points={
        'console_scripts': ['roulettenerd=rouletteNerd.main:main'],
    },
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Environment :: X11 Applications :: Qt",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=requirements,
)
